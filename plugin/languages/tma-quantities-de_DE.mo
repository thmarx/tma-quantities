��    )      d  ;   �      �     �     �     �  p   �  J   J     �     �     �  
   �     �     �                    +     3     I     ^  T   s     �     �     �       +   	  &   5     \  &   v     �  
   �     �     �     �      �     �  [     6   h  3   �  ,   �  /      4   0  ]  e     �	     �	     �	  �   
  I   �
  !   �
     �
  !     
   3     >  *   L     w     }  #   �     �     �     �     �  `   �  
   \     g     �     �  5   �  ,   �     �  +        H     a     m     r     z  '   �     �  Y   �  K   "  -   n  /   �  -   �  -   �                                           
   !            &          #      "           '                                                                   (      )       $          %                	        %MAX% = Maximum Value %MIN% = Minimum Value %STEP% = Step Value *Note - The rule with the lowest priority number will be used if multiple rules are applied to a single product. *Note - the minimum value must be greater then or equal to the step value. Above Add To Cart Activate Site Wide Rules? Below Add To Cart Categories Configuration Custom Quantity Note HTML Class Date Maximum Message Shortcode Minimum Notification Position Out of Stock Maximum Out of Stock Minimum Place in product content to display message <strong>[wpbo_quantity_message]</strong> Priority Product Quantity Rules Quantity Notification Text Roles Show Quantity Notification on Product Page? Site Wide Product Maximum Out of Stock Site Wide Product Minimum Site Wide Product Minimum Out of Stock Site Wide Step Value Step Value Tags Unit Unit of measurement Unknown / Deleted Category<br /> Update Settings You can only purchase a maximum of %s %s's at once and your cart has %s %s's in it already. You may only add a %s in multiples of %s to your cart. You may only add a maximum of %s %s's to your cart. You may only purchase %s in multiples of %s. You must add a minimum of %s %s's to your cart. Your cart must have a minimum of %s %s's to proceed. Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-05-02 08:16+0100
PO-Revision-Date: 2018-05-02 08:37+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
X-Generator: Eazy Po 0.9.5.3
 %MAX% = Maximum Wert %MIN% = Minimum Wert %STEP% = Schrittwert Hinweis - Die Regel mit der niedrigsten Priorität wird verwendet, wenn mehrere Regeln auf ein einzelnes Produkt angewendet werden. Hinweis - der Minimalwert muss größer oder gleich dem Schrittwert sein. Über "Hinzufügen zum Warenkorb" Seitenweite Regeln aktivieren? Unter "Hinzufügen zum Warenkorb" Kategorien Konfiguration Benutzerdefinierte Mengennotiz HTML-Klasse Datum Maximum Shortcode für die Benachrichtigung Minimum Benachrichtigungsposition Nicht vorrätig Maximum Nicht vorrätig Minimum Im Produktinhalt platzieren, um die Meldung <strong>[wpbo_quantity_message]</strong> anzuzeigen. Priorität Produkt Quantitäten Regeln Mengenmeldungstext Rolen Mengenbenachrichtigung auf der Produktseite anzeigen? Seitenweites Produkt Maximum nicht vorrätig Seitenweites Produkt Minimum Seitenweites Produkt Minium nicht vorrätig Seitenweiter Schrittwert Schrittwert Tags Einheit Maßeinheit Unbekannt / Gelöschte Kategorie <br /> Einstellungen übernehmen Sie können nur maximal %s %s auf einmal kaufen und Ihr Warenkorb enthält bereits %s %s. Sie können nur ein %s in Vielfachen von %s zu Ihrem Warenkorb hinzufügen. Sie können höchstens %s von %s hinzufügen. Sie können nur %s in Vielfachen von %s kaufen. Sie müssen mindestens %s von %s hinzufügen. Sie müssen mindestens %s von %s hinzufügen. 