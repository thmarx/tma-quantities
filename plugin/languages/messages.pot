# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#: includes/class-wcqu-product-unit.php:64
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-04-30 14:56+0100\n"
"PO-Revision-Date: 2018-04-30 14:57+0100\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"X-Generator: Eazy Po 0.9.5.3\n"

#: includes/class-wcqu-post-type.php:81 includes/class-wcqu-post-type.php:163
#: includes/class-wcqu-post-type.php:214
msgid "Priority"
msgstr "Priorität"

#: includes/class-wcqu-post-type.php:82 includes/class-wcqu-post-type.php:164
#: includes/class-wcqu-post-type.php:199
msgid "Minimum"
msgstr "Minimum"

#: includes/class-wcqu-post-type.php:83 includes/class-wcqu-post-type.php:165
#: includes/class-wcqu-post-type.php:202
msgid "Maximum"
msgstr "Maximum"

#: includes/class-wcqu-post-type.php:84 includes/class-wcqu-post-type.php:166
#: includes/class-wcqu-post-type.php:211
msgid "Step Value"
msgstr "Schrittwert"

#: includes/class-wcqu-post-type.php:85
msgid "Categories"
msgstr "Kategorien"

#: includes/class-wcqu-post-type.php:86
msgid "Tags"
msgstr "Tags"

#: includes/class-wcqu-post-type.php:87
msgid "Roles"
msgstr "Rolen"

#: includes/class-wcqu-post-type.php:88
msgid "Date"
msgstr "Datum"

#: includes/class-wcqu-post-type.php:205
msgid "Out of Stock Minimum"
msgstr "Nicht vorrätig Minimum"

#: includes/class-wcqu-post-type.php:208
msgid "Out of Stock Maximum"
msgstr "Nicht vorrätig Maximum"

#: includes/class-wcqu-post-type.php:217
msgid ""
"*Note - the minimum value must be greater then or equal to the step value."
msgstr ""
"Hinweis - der Minimalwert muss größer oder gleich dem Schrittwert sein."

#: includes/class-wcqu-post-type.php:218
msgid ""
"*Note - The rule with the lowest priority number will be used if multiple "
"rules are applied to a single product."
msgstr ""
"Hinweis - Die Regel mit der niedrigsten Priorität wird verwendet, wenn "
"mehrere Regeln auf ein einzelnes Produkt angewendet werden."

#: includes/class-wcqu-product-meta-box.php:29
msgid "Product Quantity Rules"
msgstr "Produkt Quantitäten Regeln"

#: includes/class-wcqu-product-unit.php:105
msgid "Unit"
msgstr "Einheit"

#: includes/class-wcqu-validations.php:101
#: includes/class-wcqu-validations.php:105
#, php-format
msgid "You must add a minimum of %s %s's to your cart."
msgstr "Sie müssen mindestens %s von %s hinzufügen."

#: includes/class-wcqu-validations.php:115
#: includes/class-wcqu-validations.php:119
#, php-format
msgid "You may only add a maximum of %s %s's to your cart."
msgstr "Sie können höchstens %s von %s hinzufügen."

#: includes/class-wcqu-validations.php:138
#: includes/class-wcqu-validations.php:142
#, php-format
msgid "You may only add a %s in multiples of %s to your cart."
msgstr "Sie können nur ein %s in Vielfachen von %s zu Ihrem Warenkorb hinzufügen."

#: includes/class-wcqu-validations.php:166
#: includes/class-wcqu-validations.php:170
#, php-format
msgid "Your cart must have a minimum of %s %s's to proceed."
msgstr "Sie müssen mindestens %s von %s hinzufügen."

#: includes/class-wcqu-validations.php:179
#: includes/class-wcqu-validations.php:183
#, php-format
msgid ""
"You can only purchase a maximum of %s %s's at once and your cart has %s %s's "
"in it already."
msgstr ""
"Sie können nur maximal %s %s auf einmal kaufen und Ihr Warenkorb enthält "
"bereits %s %s."

#: includes/class-wcqu-validations.php:199
#: includes/class-wcqu-validations.php:203
#, php-format
msgid "You may only purchase %s in multiples of %s."
msgstr "Sie können nur %s in Vielfachen von %s kaufen."